class Transmitter extends Thread{
	
	private Port<Set> p;
	
	public Set s;
	
	public Transmitter(Port p){ 
		this.p = p;
		s = new Set();
	}
	
	public void run(){
		/*Message*/ //m = new Message();
		System.out.println("Enviando el arreglo");
		p.send(s);
		System.out.println("Arreglo enviado");
	}
	
	public void setSet(Set set){
		this.s = set;
	}
}
