import static org.junit.Assert.*;
import static org.testng.AssertJUnit.assertNotNull;
import java.util.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 *Esta clase esta diseñada para realizar test al envio de numeros como mensajes.
 *@autor Arturo Angel Ticona Pinto 
 *@vercion 20/12/2017
 */
public class TestMensajeMerge 
{
	static Port<Set> p;
	static Transmitter t1;
	static Transmitter t2;
	static Receiver r;
	static Set data1;
	static Set data2;
	static int[] numbers1 = {3,6,9,10};
	static int[] numbers2 = {0,1,4,15};
	
	@BeforeClass
	public static void makeSet(){

		p = new Port<Set>();

		t1 = new Transmitter(p);
		t2 = new Transmitter(p);
		
		r = new Receiver(p);

		data1 = new Set(numbers1);
		data2 = new Set(numbers1);

		assertNotNull(numbers1);		//comprueba de que numbers no sean nulos
		assertNotNull(data1);		//comprueba de que numeros no sean nulos
		
		assertNotNull(numbers2);		//comprueba de que numbers no sean nulos
		assertNotNull(data2);		//comprueba de que numeros no sean nulos
	}

	/*
	 *	Prueba que indica que el arreglo no esta ordenado 
	 */
	
	@Test
	public void testArrayNoOrdenado(){

		int[] numbers = {8,3,5,0,6};
		int[] sorted_numbers = Arrays.copyOfRange(numbers,0,numbers.length);
		Arrays.sort(sorted_numbers);

		assertArrayEquals(sorted_numbers, numbers);

	}

	/* aqui se provara si el array que entrega merge esta ordenado */
	@Test
	public void testArrayOrdenado()
	{		
		
		int [] array_receiver = r.merge(numbers1, numbers2);
		
		int[] sorted_numbers = Arrays.copyOfRange(array_receiver,0,array_receiver.length);
		Arrays.sort(sorted_numbers);

		assertArrayEquals(array_receiver, sorted_numbers);
		
	}

	/* Provaremos si el array esta lleno de numeros o hay espacios vacios */
	@Test
	public void testNumerosArray()
	{		
		assertEquals(numbers1.length, data1.getNumbers().length);
		assertNotSame(numbers1, data1);
	}
	
	/*
	 * Verifica si el subarreglo que recibe esta ordenado
	 * */
	@Test
	public void testSubArraySorted(){
		
		assertTrue(r.isSorted(numbers1));
		
		
	}
	
	


}
