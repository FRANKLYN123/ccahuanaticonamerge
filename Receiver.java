import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

class Receiver extends Thread{
	
	private Port p;
	
	public Receiver(Port p){ 
		this.p = p; 
	}
	
	public void run(){
		
		Set m = new Set();
		System.out.println("Esperando recibir Arreglos");
		int i=0;//agregado
		int[][] aux = new int[2][];
		while(i<2)
		{
			m = (Set) p.receive();
			if(isSorted(m.numbers))
				aux[i] = m.numbers;
			else{
				System.err.println("Arreglo no ordenado");
				suspend();
			}
			System.out.println("Arreglo recibido");
			i++;
			p.reply(m);
		}
		mostrar(merge(aux[0],aux[1]));
	}

	public static void mostrar(int[] ar){
		for(int i=0; i<ar.length;i++)
			System.out.print(ar[i]+" ");
		System.out.println();
	}

	public static int[] merge(int[] a, int[] b)
	{
		int [] left = new int[a.length+1];
		int [] rigth = new int[b.length+1];
		
		for (int i = 0; i < a.length; i++)
			left[i] = a[i];
		
		
		
		for (int i = 0; i < a.length; i++)
			rigth[i] = b[i];
			
		
		left[a.length] = Integer.MAX_VALUE;
		rigth[b.length] = Integer.MAX_VALUE;
		
		
		int total = a.length + b.length; 
		int[] respuesta = new int[total];
		
		int i=0;
		int j=0;
		
		for(int k=0;k<total;k++){
			
			if( left[i]<=rigth[j]){
				respuesta[k] = left[i];
				i++;
			}else{
				respuesta[k] = rigth[j];
				j++;
			}
		}
		
		return respuesta;		
	}
	
	public boolean isSorted(int[] a){
		
		int[] sorted_numbers = Arrays.copyOfRange(a,0,a.length);
		Arrays.sort(sorted_numbers);
		
		for (int i = 0; i < a.length; i++) {
			if(a[i]!=sorted_numbers[i])
				return false;
		}
		return true;
	}
}
