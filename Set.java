public class Set extends Message {

	int[] numbers;
	
	public Set(){}
	
	public Set(int[] numbers){
		this.numbers = numbers;
	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}
	
	
	
}
