class Main{
	public static void main(String[] args){
	
		Port<Set> p = new Port<Set>();
		
		Transmitter t1 = new Transmitter(p);
		Transmitter t2 = new Transmitter(p);
		
		int [] data1 = {3,5,6,7,10,51};
		t1.setSet(new Set(data1));
	
		int [] data2 = {0,2,4,17,34,40};
		t2.setSet(new Set(data2));
		
		Receiver r = new Receiver(p);
		
		t2.start();
		t1.start();
		r.start();
		
	}
	
}
